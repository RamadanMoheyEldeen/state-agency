import { StackNavigator } from "react-navigation";
import { ContactUs } from "./ContactUs";
import { SearchResult } from "./SearchResult";
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Button
} from "react-native";
// const Stack = StackNavigator({
//   SearchResult2: { screen: SearchResult },
//   Details: { screen: ContactUs }
// });

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: "Welcome"
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Button
        title="sadfasdf"
        onPress={() => navigate("Profile", { name: "Jane" })}
      />
    );
  }
}

class ProfileScreen extends React.Component {
  static navigationOptions = {
    title: "Welcome"
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Button
        title="Go to Jane's profile"
        onPress={() => navigate("Home", { name: "Jane" })}
      />
    );
  }
}

export const Helloapp = StackNavigator({
  Search: { screen: ProfileScreen },
  Home: { screen: HomeScreen }
});
