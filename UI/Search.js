import React from "react";
import {
  StyleSheet,
  Text,
  ScrollView,
  Button,
  TouchableOpacity,
  View,
  Image,
  TextInput,
  TouchableHighlight
} from "react-native";
import { TabNavigator, StackNavigator, DrawerNavigator } from "react-navigation";
import ModalDropdown from "react-native-modal-dropdown";
import Slider from "react-native-slider";
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { Header } from "./Header";
import { SearchResult } from "./SearchResult";
import { PropertyDetails } from "./PropertyDetails";
import { SimpleTaps } from "./SimpleTaps";
import { HowItWorks } from "./HowItWorks";
import { About } from "./About";
import { Terms } from "./Terms";
import { Blogs } from "./Blogs";
import { Sell } from "./Sell";
import { Stack } from "./SearchResult";
import ModalFilterPicker from "react-native-modal-filter-picker";
import { BoxShadow } from "react-native-shadow";
import CheckBox from "react-native-check-box";
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

export class Search extends React.Component {
  constructor(props) {
    super(props);
    let index = 0;
    this.state = {
      propers: [],
      Area: [],
      areaValue: "AnyWhere",
      Compound: [],
      compoundValue: "Any",
      Property: [],
      propertyValue: "Any",
      NoBedrooms: [1,6],
      Price: [0,39],
      Areavisible: false,
      CompoundVisible: false,
      PropertyVisible: false,
      checked1: true,
      checked2: true
    };
    this.data = null;
  }

  static navigationOptions = {
    tabBarIcon: () => (
      <Image 
        source={require("../resources/images/search.png")}
        style={[styles.icon]}
      />
    ),
    drawer: () => ({
      label: 'Search'
    }),
  };

  FetchOptionsArray(propers, area, compound) {
    var areaNames = ["Anywhere"].concat(
      propers.map(proper => proper.compound_property_type.compound.area.name)
    );
    areaNames = Array.from(new Set(areaNames));
    console.log(JSON.stringify(area));
    var filteredPropers = propers;
    if (area && area != "Anywhere") {
      filteredPropers = filteredPropers.filter(proper => {
        return proper.compound_property_type.compound.area.name === area;
      });
    }
    var compoundNames = ["Any"].concat(
      filteredPropers.map(proper => proper.compound_property_type.compound.name)
    );
    compoundNames = Array.from(new Set(compoundNames));
    console.log(JSON.stringify(compound));
    if (compound && compound != "Any") {
      filteredPropers = filteredPropers.filter(proper => {
        return proper.compound_property_type.compound.name === compound;
      });
    }
    var propertyNames = ["Any"].concat(
      filteredPropers.map(proper => proper.compound_property_type.name)
    );
    propertyNames = Array.from(new Set(propertyNames));

    var Areanew = areaNames.map(function(item) {
      return { key: item, label: item };
    });

    var Compoundnew = compoundNames.map(function(item) {
      return { key: item, label: item };
    });
    var Propertynew = propertyNames.map(function(item) {
      return { key: item, label: item };
    });

    this.setState({
      propers: propers,
      Area: Areanew,
      Compound: Compoundnew,
      Property: Propertynew
    });
  }

  componentWillMount() {
    fetch("http://192.168.1.18:3000/properties", {
      headers: {
        Accept: "application/json"
      }
    })
      .then(res => res.json())
      .then(data => {
        this.data = data;
        this.FetchOptionsArray(data);
      });
      console.ignoredYellowBox = ['Warning: setState(...)'];
  
  }


  
  

  onShowArea = () => {
    this.setState({ Areavisible: true });
  };
  onShowCompound = () => {
    this.setState({ CompoundVisible: true });
  };
  onShowProperty = () => {
    this.setState({ PropertyVisible: true });
  };

  onSelectArea = picked => {
    this.setState({
      areaValue: picked,
      Areavisible: false
    });

    this.FetchOptionsArray(this.data, picked);
  };
  onSelectCompound = picked => {
    this.setState({
      compoundValue: picked,
      CompoundVisible: false
    });
    this.FetchOptionsArray(this.data, this.state.areaValue, picked);
  };
  onSelectProperty = picked => {
    this.setState({
      propertyValue: picked,
      PropertyVisible: false
    });
  };

  onCancelArea = () => {
    this.setState({
      Areavisible: false
    });
  };
  onCancelCompound = () => {
    this.setState({
      CompoundVisible: false
    });
  };
  onCancelProperty = () => {
    this.setState({
      PropertyVisible: false
    });
  };

  render() {
    const shadowOpt = {
      width: responsiveWidth(100),
      height: responsiveHeight(40),
      color: "#fff",
      border: 5,
      radius: 5,
      opacity: 0.2,
      x: 0,
      y: 3,
      style: { margin: 5, backgroundColor: "white" }
    };
    const shadowOpt2 = {
      width: responsiveWidth(100),
      height: responsiveHeight(20),
      color: "#fff",
      border: 5,
      radius: 5,
      opacity: 0.2,
      x: 0,
      y: 3,
      style: { marginLeft: 5, marginBottom: 5, backgroundColor: "white" }
    };
    const shadowOpt3 = {
        width: responsiveWidth(100),
        height:responsiveHeight(7.5),
        color:"#000",
        border:1,
        radius:2,
        opacity:0.2,
        x:2,
        y:0,
        style:{marginVertical:5,marginLeft:5}
    };
    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <ScrollView style={styles.container}>
            <BoxShadow setting={shadowOpt}>
              <Text style={styles.area}>Area: </Text>
              {console.log(JSON.stringify(this.state.Area))}
              <TouchableOpacity style={styles.Modal} onPress={this.onShowArea}>
                <Text style={styles.text}>
                  {this.state.areaValue}
                </Text>
              </TouchableOpacity>
              <ModalFilterPicker
                visible={this.state.Areavisible}
                onSelect={this.onSelectArea}
                onCancel={this.onCancelArea}
                options={this.state.Area}
                onRequestClose={this.onCancelArea}
              />

              <Text style={styles.area}>Compound: </Text>
              <TouchableOpacity
                style={styles.Modal}
                onPress={this.onShowCompound}
              >
                <Text style={styles.text}>
                  {this.state.compoundValue}
                </Text>
              </TouchableOpacity>
              <ModalFilterPicker
                visible={this.state.CompoundVisible}
                onSelect={this.onSelectCompound}
                onCancel={this.onCancelCompound}
                options={this.state.Compound}
                onRequestClose={this.onCancelCompound}
              />
              <Text style={styles.area}>Property Type: </Text>
              <TouchableOpacity
                style={styles.Modal}
                onPress={this.onShowProperty}
              >
                <Text style={styles.text}>
                  {this.state.propertyValue}
                </Text>
              </TouchableOpacity>
              <ModalFilterPicker
                visible={this.state.PropertyVisible}
                onSelect={this.onSelectProperty}
                onCancel={this.onCancelProperty}
                options={this.state.Property}
                onRequestClose={this.onCancelProperty}
              />
            </BoxShadow>
            <BoxShadow setting={shadowOpt2}>
              <Text style={styles.area}>Number of Bedrooms: </Text>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <MultiSlider
                  values={[this.state.NoBedrooms[0], this.state.NoBedrooms[1]]}
                  sliderLength={280}
                  onValuesChange={(values)=>this.setState({
                    NoBedrooms: values,
                  })}
                  min={1}
                  max={6}
                  step={1}
                  allowOverlap
                  containerStyle={{marginLeft:15}}
                  trackStyle={{backgroundColor:'lightblue',height:5}}
                  selectedStyle={{backgroundColor:'lightblue'}}
                  unselectedStyle={{backgroundColor: 'grey'}}
                  markerStyle={{backgroundColor:'grey',height:20,width:20,borderRadius:15}}
                />
                <Text style={styles.values}>
                  {this.state.NoBedrooms[0]}:{this.state.NoBedrooms[1]}
                </Text>
              </View>
              <Text style={styles.area}>Price Range: </Text>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <MultiSlider
                  values={[this.state.Price[0], this.state.Price[1]]}
                  sliderLength={280}
                  onValuesChange={(values)=>this.setState({
                    Price: values,
                  })}
                  min={0}
                  max={39.3}
                  step={0.5}
                  allowOverlap
                  containerStyle={{marginLeft:15}}
                  trackStyle={{backgroundColor:'lightblue',height:5}}
                  selectedStyle={{backgroundColor:'lightblue'}}
                  unselectedStyle={{backgroundColor: 'grey'}}
                  markerStyle={{backgroundColor:'grey',height:20,width:20,borderRadius:15}}
                />
                <Text style={{fontSize:11,fontWeight:'bold',marginLeft:5,marginTop:3}}>
                  {this.state.Price[0]}:{this.state.Price[1]} M
                </Text>
              </View>
            </BoxShadow>
            <View
              style={{
                flexDirection: "row",
                margin: 5,
                backgroundColor: "white"
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  margin: 10,
                  backgroundColor: "white"
                }}
              >
                <Text style={{ fontSize: 15, fontWeight: 'bold' }}>
                  New Sale{" "}
                </Text>
                <CheckBox
                  onClick={() =>
                    this.setState({ checked1: !this.state.checked1 })}
                  isChecked={this.state.checked1}
                />
              </View>
              <View
                style={{
                  flexDirection: "row",
                  margin: 10,
                  backgroundColor: "white",
                  marginLeft: 50
                }}
              >
                <Text style={{ fontSize: 15, fontWeight: 'bold' }}>
                  Resale{" "}
                </Text>
                <CheckBox
                  onClick={() =>
                    this.setState({ checked2: !this.state.checked2 })}
                  isChecked={this.state.checked2}
                />
              </View>
            </View>
          </ScrollView>
          <BoxShadow setting={shadowOpt3}>
          <TouchableHighlight style={{
                    position:"relative",
                    backgroundColor: "#fff",
                    borderRadius:4
                }}>
          <View style={{flexDirection: "row"}}>
            <TouchableOpacity
              onPress={() => {this.props.navigation.navigate("Result")}}
              style={styles.button}
            >
              <Text style={styles.search}>SEARCH PROPERTY</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  areaValue: "AnyWhere",
                  compoundValue: "Any",
                  propertyValue: "Any",
                  NoBedrooms: [1,6],
                  Price: [0,39],
                  checked1: true,
                  checked2: true
                })}
              style={styles.Resetbutton}
            >
              <Text style={styles.search}>Reset Filter</Text>
            </TouchableOpacity>
            </View>
            </TouchableHighlight>
            </BoxShadow>
        </View>
      </View>
    );
  }
}

export const StackSearch = StackNavigator({
  Search: {screen: Search,
    navigationOptions:()=>({
    header: ({ navigation }) => (
        <View style={styles.container2}>
          <TouchableOpacity onPress={() =>  navigation.navigate('DrawerOpen')}>
            <Image style={{width:50, height:50}} source={require("../resources/images/menu2.png")}/>
          </TouchableOpacity>
          <View style={styles.img}>
            <Image source={require("../resources/images/logo.png")} />
          </View>
        </View>
    )}
    )},
  Result: { screen: SearchResult,
  navigationOptions:()=>({
    tabBarIcon: () => (
      <Image 
        source={require("../resources/images/search.png")}
        style={styles.icon}/>
    )
    })
    },
  Details: { screen: PropertyDetails,
  navigationOptions:()=>({
    tabBarIcon: () => (
      <Image 
        source={require("../resources/images/search.png")}
        style={styles.icon}/>
    )
    })
    }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  container2: {
    flexDirection: "row",
    height: responsiveHeight(10),
    alignItems: "center",
    backgroundColor: "#0080ff"
  }, 
  header:{
    marginTop: -200
  },
  more: {
    flex: 1
  },
  img: {
    flex: 1,
    marginLeft:80
  },
  Modal: {
    width: responsiveWidth(90),
    marginLeft: 10,
    backgroundColor: "white",
    borderWidth: 2,
    borderRadius: 4,
    borderColor: "grey"
  },
  dropdown: {
    width: responsiveWidth(90),
    height: responsiveHeight(10)
  },
  text: {
    fontSize: responsiveFontSize(2),
    paddingTop: 8,
    paddingBottom: 8,
    marginLeft: 10
  },
  dropDownText: {
    backgroundColor: "lightgrey",
    color: "black",
    fontSize: responsiveFontSize(2)
  },
  area: {
    fontSize: responsiveFontSize(2),
    margin: 10,
    fontWeight: "bold"
  },
  button: {
    backgroundColor: "#d35400",
    width: responsiveWidth(50),
    height: responsiveHeight(6),
    margin: 10,
    alignItems: "center"
  },
  Resetbutton: {
    backgroundColor: "#bdc3c7",
    width: responsiveWidth(35),
    height: responsiveHeight(6),
    margin: 10,
    alignItems: "center",
    marginRight: 5
  },
  search: {
    fontSize: responsiveFontSize(2.5),
    fontWeight: "bold",
    marginTop: 10
  },
  values: {
    fontSize: responsiveFontSize(2),
    marginLeft: 10,
    fontWeight: "bold"
  },
  icon: {
    width: 26,
    height: 26
  }
});


/*componentDidMount() {
  fetch('http://192.168.1.20:3000/properties', {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    areaValue: this.state.areaValue,
    compoundValue: this.state.compoundValue,
    propertyValue: this.state.propertyValue,
    NoBedrooms: this.state.NoBedrooms,
    Price: this.state.Price,
    checked1: this.state.checked1,
    checked2: this.state.checked2,
  })
})
  }*/

  /*<TouchableOpacity
              onPress={() => {this.props.navigation.navigate("Result")
              fetch('http://192.168.1.20:3000/properties', {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  areaValue: this.state.areaValue,
                  compoundValue: this.state.compoundValue,
                  propertyValue: this.state.propertyValue,
                  NoBedrooms: this.state.NoBedrooms,
                  Price: this.state.Price,
                  checked1: this.state.checked1,
                  checked2: this.state.checked2,
                })
              })
      }}*/

