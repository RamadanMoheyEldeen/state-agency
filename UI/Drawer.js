import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { SimpleTaps } from "./SimpleTaps";
import { StackSearch } from "./Search";
import { HowItWorks } from "./HowItWorks";
import { About } from "./About";
import { Terms } from "./Terms";
import { Sell } from "./Sell";
import { Blogs } from './Blogs';
import { TabNavigator, DrawerNavigator } from "react-navigation";

const Drawer = DrawerNavigator({
  "Home": {
    screen: SimpleTaps,
    navigationOptions: {
      drawer: () => ({
        label: "Home"
      })
    }
  },
  "How It Works": {
    screen: HowItWorks,
    navigationOptions: {
      drawer: () => ({
        label: "HowItWorks"
      })
    }
  },
   "About Cooing": {
    screen: About,
    navigationOptions: {
      drawer: () => ({
        label: "About"
      })
    }
  },
  "Blogs": {
    screen: Blogs,
    navigationOptions: {
      drawer: () => ({
        label: "Blogs"
      })
    }
  },
  "Terms And Conditions": {
    screen: Terms,
    navigationOptions: {
      drawer: () => ({
        label: "Terms"
      })
    }
  },
  
});

export default Drawer;
