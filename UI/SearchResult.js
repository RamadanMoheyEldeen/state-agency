import React, { Navigator } from "react";
import {
	StyleSheet,
	Text,
	View,
	Image,
	ScrollView,
	TouchableOpacity,
	Button
} from "react-native";
import { TabNavigator } from "react-navigation";
import { StackNavigator } from "react-navigation";
import { Header } from "./Header";
import { ContactUs } from "./ContactUs";
import { Font } from "expo";
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

export class SearchResult extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			propers: []
		};
	}

	static navigationOptions = {
		tabBarIcon: () => (
			<Image 
				source={require("../resources/images/search.png")}
				style={[styles.icon]}
			/>
		)
	}

	componentWillMount() {
		fetch("http://192.168.1.20:3000/properties", {
			headers: {
				Accept: "application/json"
			}
		})
			.then(res => res.json())
			.then(propers => this.setState({ propers }));
	}

	render() {
		return (
			<View style={styles.container}>
				<Header navigation={this.props.navigation}/>
				<ScrollView>
					<View>
						{this.state.propers.map((proper, i) =>
							<View key={i}>
								<TouchableOpacity
									onPress={() =>
										this.props.navigation.navigate(
											"Details",
											{
												id: proper.id,
												compound: proper.compound_property_type.compound.id,
											}
										)}
									activeOpacity={1}	
								>
									<Image
										source={{ uri: proper.image_path }}
										style={styles.image}
									>
										<Text style={styles.property}>
											{proper.name.toUpperCase()}
										</Text>
										<Text style={styles.details}>
											{proper.compound_property_type.compound.name.toUpperCase()}
										</Text>
										<Text style={styles.details}>
											{
												proper.compound_property_type
													.compound.area.name
											}
										</Text>
										<Text style={styles.details}>
											By:{" "}
											{
												proper.compound_property_type
													.compound.developer.name
											}
										</Text>
										<Text style={styles.area}>
											{" "}{proper.max_unit_area} m{" "}
											<Text style={styles.power}>2</Text>
										</Text>
										<Text style={styles.price}>
											{" "}{proper.price_text}{" "}
											{proper.currency_text}
										</Text>
										<Text style={styles.button}>
											{" "}VIEW PROPERTY
										</Text>
									</Image>
								</TouchableOpacity>
							</View>
						)}
					</View>
				</ScrollView>
			</View>
		);
	}
}

export const Stack = StackNavigator({
	Search: { screen: SearchResult },
	Details: { screen: ContactUs }
});

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	image: {
		height: responsiveHeight(37),
		margin: 10
	},
	property: {
		color: "white",
		fontSize: responsiveFontSize(2.5),
		fontWeight: "bold",
		marginLeft: 5,
		marginTop: 10
	},
	details: {
		color: "white",
		marginLeft: 5
	},
	price: {
		color: "white",
		fontWeight: "bold",
		marginTop: 20
	},
	area: {
		fontSize: responsiveFontSize(2),
		marginTop: 50,
		marginLeft: 5,
		fontWeight: "bold",
		color: "white"
	},
	power: {
		fontSize: responsiveFontSize(1.5)
	},
	button: {
		alignItems: "center",
		justifyContent:'center',
		fontWeight:'bold',
		marginLeft: 200,
		padding: 3,
		marginRight:10,
		color: "white",
		backgroundColor: "#d35400"
	}
});
