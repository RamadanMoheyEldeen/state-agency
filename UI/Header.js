import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  ScrollView
} from "react-native";
import { StackNavigator } from "react-navigation";
import { SimpleTaps } from "./SimpleTaps";
import { Search } from "./Search";
import { HowItWorks } from "./HowItWorks";
import { About } from "./About";
import { Terms } from "./Terms";
import { Blogs } from "./Blogs";
import { Sell } from "./Sell";
import { TabNavigator, DrawerNavigator } from "react-navigation";




export const Header = ({ navigation }) => (
  <View style={styles.container}>
    <TouchableOpacity onPress={() =>  navigation.navigate('DrawerOpen')}>
      <Image style={{width:50, height:50}} source={require("../resources/images/menu2.png")}/>
    </TouchableOpacity>
    <View style={styles.img}>
      <Image source={require("../resources/images/logo.png")} />
    </View>
  </View>
);




const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    height: 70,
    alignItems: "center",
    backgroundColor: "#0080ff"
  },
  more: {
    flex: 1
  },
  img: {
    flex: 1,
    marginLeft:80
  }
});

/*export class Header extends React.Component {
  render({navigation}) {
    console.log(this.props)
    return (
      <View style={styles.container}>
          <TouchableOpacity onPress={() =>  navigation.navigate('DrawerOpen')}>
            <Image style={{width:50, height:50}} source={require("../resources/images/menu2.png")}/>
          </TouchableOpacity>
        <View style={styles.img}>
          <Image source={require("../resources/images/logo.png")} />
        </View>
      </View>
    );
  }
}*/

/*SimpleTaps: {
    screen: SimpleTaps,
    navigationOptions: {
      drawer: () => ({
        label: "Home"
      })
    }
  },*/

  /*const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    height: 70,
    alignItems: "center",
    backgroundColor: "#0080ff"
  },
  more: {
    flex: 1
  },
  img: {
    flex: 1,
    marginLeft:80
  }
});*/