import React, { Component } from 'react';
import { WebView } from 'react-native';
import { Header } from "./Header";

export class Blogs extends Component {
	constructor(props) {
		super(props);
	}
 	 render() {
	    return (
	     	<WebView
		        source={{uri: 'https://www.cooingestate.com/blog'}}
		        style={{marginTop: 20}}
		    /> 
    );
  }
}