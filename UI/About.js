import React, { Component } from 'react';
import { WebView } from 'react-native';
import { Header } from "./Header";

export class About extends Component {
	constructor(props) {
		super(props);
	}
 	 render() {
	    return (
	     	<WebView
		        source={{uri: 'https://www.cooingestate.com/about', footer: false}}
		        style={{marginTop: 20}}
		    /> 
    );
  }
}