import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { Header } from "./Header";

export class Areas extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<View>
				<Header navigation={this.props.navigation}/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,

		backgroundColor: "#fff",
		alignItems: "center",
		justifyContent: "center"
	}
});
