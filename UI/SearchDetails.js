import React from "react";
import { StyleSheet, Text, View, ScrollView, Image } from "react-native";
import { TabNavigator } from "react-navigation";
import { Header } from "./Header";

export class ContactUs extends React.Component {
	constructor() {
		super();
		this.state = {
			details: {}
		};
	}

	componentDidMount() {
		fetch("http://192.168.1.18:3000/properties/521", {
			headers: {
				Accept: "application/json"
			}
		})
			.then(res => res.json())
			.then(details => this.setState({ details }));
	}

	render() {
		return (
			<ScrollView>
				<Header />
				<View>
					<Text>this.props.navigator.navigate.id</Text>
					<Image
						source={{
							uri:
								"http://www.cooingestate.com/uploads/img/properties/gallery/original_quality/r3QsFRUybM1ey5VeHpy8K9av2IzWqS9.jpg"
						}}
						style={{ width: 320, height: 200 }}
					/>
					<Text style={{ paddingLeft: 10, paddingTop: 10 }}>
						{" "}PROPERTY OVERVIEW
						{this.state.details}
					</Text>
				</View>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
		alignItems: "center",
		justifyContent: "center"
	}
});
