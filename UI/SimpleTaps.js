import React from "react";
import { StyleSheet, Text, View, Button, Image } from "react-native";
import { StackHome } from "./Home";
import { ContactUs } from "./ContactUs";
import { StackSearch } from './Search' 
import { Sell } from "./Sell";
import { TabNavigator } from "react-navigation";


export const SimpleTaps = TabNavigator(
  {
    Home:{screen: StackHome},
    Search: { screen: StackSearch },
    "Contact Us": { screen: ContactUs },
    Sell: {screen: Sell}
  },
  {
    tabBarPosition: "bottom",
    swipeEnabled: false,
    tabBarOptions:{
      showIcon: true,
      showLabel:false
    }
  }
);
