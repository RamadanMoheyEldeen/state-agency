import React from "react";
import {
    StyleSheet,
    Text,
    View,
    Button,
    Image,
    TouchableOpacity,
    TextInput ,Animated, Keyboard,ScrollView, Platform
} from "react-native";
import { Header } from "./Header";
export class AreaDetails extends React.Component {
    constructor(props) {
        super(props);
            this.state = {
            propers: [],
            filteredPropers:[]
        };
    }
        componentWillMount() {
        fetch("http://192.168.1.20:3000/properties", {
            headers: {
                Accept: "application/json"
            }
        })
            .then(res => res.json())
            .then(propers => {this.setState({ propers })
            var filteredPropers = propers;  
            filteredPropers = filteredPropers.filter(proper => {
            return proper.compound_property_type.compound.area.id === this.props.navigation.state.params.id;
            });
            this.setState({
                filteredPropers: filteredPropers
            });     
        });
    }
    render() {
        return (
            <View style={styles.container}>
            <Header navigation={this.props.navigation}/>
                    <Text style={styles.name}>{this.props.navigation.state.params.name}</Text>
                    <View style={{backgroundColor: 'lightgrey',margin:3,height:130}}>
                        <ScrollView>
                            <Text style={{margin: 5, fontSize:15}}>{this.props.navigation.state.params.description}</Text>
                        </ScrollView>
                    </View>
                    <ScrollView>
                    <View style={{backgroundColor:'lightgrey',margin:3,marginTop:10}}>
                        <Text style={{fontSize:15,margin:5}}>Displaying all results from {this.props.navigation.state.params.name}</Text>
                    </View>
                    <View>
                        {this.state.filteredPropers.map((proper, i) =>
                            <View key={i}>
                                <TouchableOpacity
                                    onPress={() =>
                                        this.props.navigation.navigate(
                                            "Details",
                                            {
                                                id: proper.id,
                                                compound: proper.compound_property_type.compound.id,
                                            }
                                        )}
                                    activeOpacity={1}   
                                >
                                    <Image
                                        source={{ uri: proper.image_path }}
                                        style={styles.image}
                                    >
                                        <Text style={styles.property}>
                                            {proper.name.toUpperCase()}
                                        </Text>
                                        <Text style={styles.details}>
                                            {proper.compound_property_type.compound.name.toUpperCase()}
                                        </Text>
                                        <Text style={styles.details}>
                                            {
                                                proper.compound_property_type
                                                    .compound.area.name
                                            }
                                        </Text>
                                        <Text style={styles.details}>
                                            By:{" "}
                                            {
                                                proper.compound_property_type
                                                    .compound.area.name
                                            }
                                        </Text>
                                        <Text style={styles.area}>
                                            {" "}{proper.max_unit_area} m{" "}
                                            <Text style={styles.power}>2</Text>
                                        </Text>
                                        <Text style={styles.price}>
                                            {" "}{proper.price_text}{" "}
                                            {proper.currency_text}
                                        </Text>
                                        <Text style={styles.button}>
                                            {" "}VIEW PROPERTY
                                        </Text>
                                    </Image>
                                </TouchableOpacity>
                            </View>
                        )}
                        </View>
                </ScrollView>
            </View>
        );
    } 
}   
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    name:{
        fontSize: 20,
        fontWeight: 'bold',
        color: '#1565C0',
        marginLeft:5,
    },
    image: {
        height: 230,
        margin: 10
    },
    image: {
        height: 230,
        margin: 10
    },
    property: {
        color: "white",
        fontSize: 18,
        fontWeight: "bold",
        marginLeft: 5,
        marginTop: 10
    },
    details: {
        color: "white",
        marginLeft: 5
    },
    price: {
        color: "white",
        fontWeight: "bold",
        marginTop: 20
    },
    area: {
        fontSize: 15,
        marginTop: 50,
        marginLeft: 5,
        fontWeight: "bold",
        color: "white"
    },
    power: {
        fontSize: 10
    },
    button: {
        alignItems: "center",
        justifyContent:'center',
        fontWeight:'bold',
        marginLeft: 200,
        padding: 3,
        marginRight:10,
        color: "white",
        backgroundColor: "#d35400"
    }
});