import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  TouchableOpacity,TouchableHighlight ,
  TextInput ,Animated, Keyboard,ScrollView, Platform ,ToastAndroid
} from "react-native";
import * as  t from 'tcomb-form-native';
import { ImagePicker } from "expo";
import { Header } from "./Header";
import ModalFilterPicker from "react-native-modal-filter-picker";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import * as EmailValidator from 'email-validator';
import CameraRollPicker from 'react-native-camera-roll-picker';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';



const Form = t.form.Form ;
const phone = t.refinement(t.Number, function (n) { return n.toString().length >= 11 });
const email = t.refinement(t.String, function (mail) { return EmailValidator.validate(mail); });
const  sell = t.struct({
    unitName:t.String,
    comment:t.maybe(t.String),
    yourName: t.String,
    phone: phone,
    email:email ,
    });
const options = {  auto: 'none' ,  
                  fields: {
                  unitName: {
                    placeholder: 'Give Name to the unit',
                    returnKeyType: 'next'
                  },
                  comment: {
                    placeholder: 'Do you have a comment (Optional)',
                    returnKeyType: 'next',
                  },
                  yourName: {
                    placeholder: 'Your Name',
                   returnKeyType: 'next'

                  } ,
                  phone:{
                    placeholder: 'Your Phone Number (max 14)',
                    error: "Enter a valid phone number",
                    returnKeyType: 'next',
                    maxLength: 14 ,
                    // EmailValidator.validate("ag@mail.com")? "Hello" : "World"
                   },
                  email:{
                    placeholder: 'Your Email',
                    keyboardType: 'email-address',
                    error: 'Insert a valid email' ,
                    returnKeyType: 'go',

                  }
                }
    };

export class Sell extends React.Component {
  constructor() {
    super();
    let index = 0;
    this.state = {
      propers: [],
      Compound: [{ key: "Select Compound", label: "Select Compound" }],
      compoundValue: "Select Compound",
      Property: [
        { key: "Select Property Type", label: "Select Property Type" }
      ],
      propertyValue: "Select Property Type",
      Areavisible: false,
      CompoundVisible: false,
      PropertyVisible: false,
      keyboardHeight: new Animated.Value(0),
      flag: false,
      warning: "",
      value: {} ,
      confirm:"",

    };
  }


      getInitialState() {
      return {
        value: {
            unitName:"",
            comment: "",
            yourName: "",
            phone: "" ,
            email: "",
        }
      };
    }

   onChange(value) {
      console.log(value);
        this.setState({value : value});
  }


    animateKeyboardHeight = (toValue, duration) => {
    Animated.timing(
      this.state.keyboardHeight,
      {toValue, duration},
    ).start();
  };
  static navigationOptions = {
    tabBarIcon: () => (
      <Image 
        source={require("../resources/images/sell.png")}
        style={[styles.icon]}
      />
    )
  }


 
  clearForm() {
    // clear content from all textbox
    this.setState({ value: null });
  }

  onPress(){
    // call getValue() to get the values of the form
    const value = this.refs.form.getValue();
    if(this.state.compoundValue ==  "Select Compound" || this.state.propertyValue == "Select Property Type"){
      this.setState({warning: "Please Choose Compound and Type"})
    }
    else
    {
      this.setState({flag:true})
      this.setState({warning: ""})
    }

    if (value && this.state.flag) { // if validation fails, value will be null
      console.log(value); // value here is an instance of Person
     this.clearForm()
     this.setState({compoundValue: "Select Compound"})
     this.setState({propertyValue: "Select Property Type"})
     this.setState({warning: ""})
     this.setState({flag: false})
     ToastAndroid.show(' Your form has been submitted successfully ', ToastAndroid.SHORT , ToastAndroid.TOP);


    }


  }

  componentWillMount() {

    if (Platform.OS === "android") {
      this.keyboardShowListener = Keyboard.addListener("keyboardDidShow", ({endCoordinates}) => {
        this.animateKeyboardHeight(endCoordinates.height, 0)
      });
      this.keyboardHideListener = Keyboard.addListener("keyboardDidHide", () => {
        this.animateKeyboardHeight(0, 300)
      })
    }

      scrollToInput = (reactNode) => {
    this.view.scrollToFocusedInput(reactNode)
  };

  handleOnFocus = (e) => {
    if (Platform.OS === "android") {
      this.scrollToInput(ReactNative.findNodeHandle(e.target))
    }
  };



    var Details = propers => {
      var compoundNames = propers.map(
        proper => proper.compound_property_type.compound.name
      );
      compoundNames = Array.from(new Set(compoundNames));
      var propertyNames = propers.map(
        proper => proper.compound_property_type.name
      );
      propertyNames = Array.from(new Set(propertyNames));

      var Compoundnew = compoundNames.map(function(item) {
        return { key: item, label: item };
      });
      var Propertynew = propertyNames.map(function(item) {
        return { key: item, label: item };
      });

      this.setState({
        propers: propers,
        Compound: Compoundnew,
        Property: Propertynew
      });
    };


    fetch("http://192.168.1.20:3000/properties", {
  headers: {
        Accept: "application/json"
      }
    })
      .then(res => res.json())
      .then(Details);
  }

  onShowCompound = () => {
    this.setState({ CompoundVisible: true });
  };
  onShowProperty = () => {
    this.setState({ PropertyVisible: true });
  };

  onSelectCompound = picked => {
    this.setState({
      compoundValue: picked,
      CompoundVisible: false
    });
  };
  onSelectProperty = picked => {
    this.setState({
      propertyValue: picked,
      PropertyVisible: false
    });
  };

  onCancelCompound = () => {
    this.setState({
      CompoundVisible: false
    });
  };
  onCancelProperty = () => {
    this.setState({
      PropertyVisible: false
    });
  };

  render() {
    let { image } = this.state;
    console.log(JSON.stringify(this.state.Compound));

    return (
    <View style={styles.container}>
        <View style={styles.header}>
          <Header navigation={this.props.navigation}/>
        </View>
      <KeyboardAwareScrollView
          extraScrollHeight={60}
              style={styles.container}
              enableOnAndroid
              extraHeight={Platform.OS === "android" ? 10 : undefined}
              enableResetScrollToCoords 
          >


        <View style={styles.about_unit}>
          <Text style={styles.warning}>{this.state.warning}</Text>
          <View style={styles.modals}>
            <View style={styles.column}>
              <Text style={styles.title}>Compound </Text>
              <TouchableOpacity
                style={styles.Modal}
                onPress={this.onShowCompound}
              >
                <Text style={styles.text}>
                  {this.state.compoundValue}
                </Text>

              </TouchableOpacity>
              <ModalFilterPicker
                visible={this.state.CompoundVisible}
                onSelect={this.onSelectCompound}
                onCancel={this.onCancelCompound}
                options={this.state.Compound}
              />
            </View>
            <View style={styles.column}>
              <Text style={styles.title}>Property Type</Text>
              <TouchableOpacity
                style={styles.Modal}
                onPress={this.onShowProperty}
              >
                <Text style={styles.text}>
                  {this.state.propertyValue}
                </Text>
              </TouchableOpacity>
              <ModalFilterPicker
                visible={this.state.PropertyVisible}
                onSelect={this.onSelectProperty}
                onCancel={this.onCancelProperty}
                options={this.state.Property}
              />
            </View>
          </View>


          <View style={styles.upload}>
            <Button
              title=" Upload Property Image From Gallery "
              onPress={this._pickImage}
            />

            {image &&
              <Image
                source={{ uri: image }}
                style={{ width: 30, height: 30 }}
              />}
          </View>
        </View>
   <View style={styles.about_owner}>
             <Form
                  ref="form"
                  type={sell}
                  options={options}
                  value={this.state.value}
                  context={{locale: 'it-IT'}}
                  onChange={(d)=> this.onChange(d)}
             />

         <Text style={styles.confirm}>{this.state.confirm}</Text>
          <View style={{ alignItems: "center" }}>
            <TouchableHighlight style={styles.submit} onPress={() => this.onPress() } underlayColor='#99d9f4'>
              <Text style={{color: 'white' , fontWeight:'bold' , fontSize: responsiveFontSize(3)}}> Submit </Text>
            </TouchableHighlight>
          </View>
        </View>

        <Animated.View style={{height: this.state.keyboardHeight}}/>
        </KeyboardAwareScrollView>
        </View>

      
    ); // end of return
  } // end of render function

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      multiple: true,
      aspect: [4, 3]
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
  };
} // end of the class compononet

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  firstView: {
    flex: 1
  },
  header: {
    flex: 0.15,
    backgroundColor: "white"
  },
  about_unit: {
    flex: 1.18,
    backgroundColor: "white",
    margin: 5
  },
  about_owner: {
    flex:.9 ,
    backgroundColor: "white",
    padding:15
  },
  text: {
    fontSize: 12,
    paddingTop: 3,
    paddingBottom: 3,
    margin: 5
  },
  title: {
    fontWeight: "bold",
    fontSize: responsiveFontSize(2),
    paddingTop: 3,
    paddingBottom: 3,
    margin: 5
  },
  Modal: {
    width: responsiveWidth(46),
    height: responsiveHeight(6),
    marginLeft: 5,
    paddingLeft:10,
    backgroundColor: "white",
    borderWidth: 1,
    borderRadius: 4
  },
  modals: {
    flexDirection: "row"
  },
  column: {
    flexDirection: "column"
  },
  upload: {
    flex: 1,
    marginTop: 25,
    marginLeft: 5,
    marginRight: 5
  },
  nameComment: {
    marginTop: 30,
    flexDirection: "row"
  },
  userText: {
    margin: 5,
    borderWidth: 1,
    backgroundColor: "white",
    height: 40,
    padding: 10,
    borderRadius: 4
  },
   submit: {
    margin: 5,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#d35400",
    borderRadius: 4,
    width: responsiveWidth(50),
    height: responsiveHeight(10),
    marginBottom: 5
  },
  icon: {
    width: 26,
    height: 26
  } ,
  warning:{
    color:"red",
    fontSize:17,
  }
});



                  // <CameraRollPicker callback={this.getSelectedImages} />
