import React from "react";
import Communications from "react-native-communications";
import {
	StyleSheet,
	Text,
	View,
	ScrollView,
	Image,
	Button,
	TouchableOpacity,
	TouchableHighlight
} from "react-native";
import { TabNavigator, StackNavigator } from "react-navigation";
import { Header } from "./Header";
import ImageSlider from "react-native-image-slider";
import Lightbox from "react-native-lightbox";
import Swiper from "react-native-swiper";
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';


export class PropertyDetails extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			propers: [],
			details: {},
			pics: [],
			filteredPropers:[],
		};
	}

	static navigationOptions = {
		tabBarIcon: () => (
			<Image 
				source={require("../resources/images/search.png")}
				style={[styles.icon]}
			/>
		)
	}

	componentWillMount() {
		//this.props.navigation.state.params.id
		fetch(
			"http://192.168.1.20:3000/properties/" +
				this.props.navigation.state.params.id,
			{
				headers: {
					Accept: "application/json"
				}
			}
		)
			.then(res => res.json())
			.then(details => this.setState({ details }));

		fetch("http://192.168.1.20:3000/properties", {
			headers: {
				Accept: "application/json"
			}
		})
			.then(res => res.json())
			.then(propers => {this.setState({ propers })
	
				filteredPropers = propers.filter(proper => {
	        	return proper.compound_property_type.compound.id === this.props.navigation.state.params.compound;
	      		});


	      		this.setState({
		      		filteredPropers: filteredPropers
		    	});
				

			});
	}

	componentWillUnmount() {
		clearInterval(this.state.interval);
	}

	render() {
		if (!this.state.details.id) {
			return <View />;
		} else {
			console.log(this.state.details);
		}

		return (
			<View style={styles.container}>
					<Header navigation={this.props.navigation}/>
				<ScrollView style={styles.main}>
					<View style={styles.container}>
						<Swiper
							style={styles.wrapper}
							showsButtons={true}
							autoplay={true}
						>
							{this.state.details.images.map((image, index) =>
								<Image
									key={index}
									source={{
										uri:
											"http://www.cooingestate.com/" +
											image.image_path
									}}
									style={{ height: 230, margin: 10 }}
								/>
							)}
						</Swiper>

						<View style={styles.box3}>
							<Text style={{ margin: 15 }}>
								{this.state.details &&
									this.state.details.compound_property_type
										.compound.seo_description}
							</Text>
						</View>
					</View>

					<View style={styles.box2}>
						<View style={styles.imageText}>
							<Image
								style={styles.img}
								source={{
									uri:
										"http://www.cooingestate.com/build/assets/mobile/img/ic_years_payment@2x.png"
								}}
							/>
							<Text style={{ fontSize: 25, fontWeight: 'bold' }}>
								{this.state.details &&
									"Price: " +
										this.state.details.price_text}{" "}
								{this.state.details &&
									this.state.details.currency_text}
							</Text>
						</View>

						<View style={styles.area}>
							<View style={styles.imageText}>
								<Image
									style={styles.img}
									source={{
										uri:
											"http://www.cooingestate.com/build/assets/mobile/img/ic_m2@3x.png"
									}}
								/>
								<Text style={{ fontWeight: 'bold' }}>
									{this.state.details &&
										"Unit area:" +
											this.state.details.max_unit_area}
								</Text>
							</View>

							<View style={styles.imageText}>
								<Image
									style={styles.img}
									source={{
										uri:
											"http://www.cooingestate.com/build/assets/mobile/img/ic_building@3x.png"
									}}
								/>

								<Text style={{ fontWeight: 'bold' }}>
									{this.state.details &&
										this.state.details.number_of_floors +
											" Floors"}
								</Text>
							</View>
						</View>
						<View style={styles.bed}>
							<View style={styles.imageText}>
								<Image
									style={styles.img}
									source={{
										uri:
											"http://www.cooingestate.com/build/assets/mobile/img/ic_bed@3x.png"
									}}
								/>
								<Text style={{ fontWeight: 'bold' }}>
									{this.state.details &&
										this.state.details.number_of_bedrooms +
											" Bedrooms"}
								</Text>
							</View>

							<View style={styles.imageText}>
								<Image
									style={styles.img}
									source={{
										uri:
											"http://www.cooingestate.com/build/assets/mobile/img/ic_path@3x.png"
									}}
								/>
								<Text style={{ fontWeight: 'bold' }}>
									{this.state.details &&
										this.state.details.number_of_bathrooms +
											" Bathrooms"}
								</Text>
							</View>
						</View>

						<View style={styles.imageText}>
							<Image
								style={styles.img}
								source={require("../resources/images/date.png")}
							/>
							<Text style={{ fontSize: 20, fontWeight: 'bold' }}>
								{this.state.details &&
									this.state.details.ready_by}
							</Text>
						</View>

						<View style={styles.map}>
							<Text
								style={{
									fontSize: 20,
									color: "#0080ff",
									fontWeight: 'bold'
								}}
							>
								{"Properity Location"}
							</Text>
						</View>

						<Lightbox navigator={this.props.navigator}>
							<Image
								source={{
									uri:
										"http://www.cooingestate.com/" +
										this.state.details
											.compound_property_type.compound
											.map_path
								}}
								style={{ height: 400, margin: 10 }}
							/>
						</Lightbox>
					</View>
					<View style={{alignItems:'center'}}>
						<Text style={{fontSize:20,fontWeight:'bold',margin:5}}>RELATED RESULTS</Text>
					</View>
					<View style={{backgroundColor:'white', margin:3}}>
					<Swiper
						style={{height:200,margin:5}}
						showsButtons={true}
						autoplay={true}
					>
						{this.state.filteredPropers.map((proper, i) =>
								<View key={i}>
									<TouchableOpacity
										style={{height:150,margin:5}}
										onPress={() =>
											this.props.navigation.navigate(
												"Details",{
												id: proper.id,
												compound: proper.compound_property_type.compound.name,
												compoundID: proper.compound_property_type.compound.id,
												area: proper.compound_property_type.compound.area.id,
											}
											)}
										activeOpacity={1}	
									>
										<Image
										source={{ uri: proper.image_path }}
										style={styles.image}
									>
										<Text style={styles.property}>
											{proper.name.toUpperCase()}
										</Text>
										<Text style={styles.details}>
											{proper.compound_property_type.compound.name.toUpperCase()}
										</Text>
										<Text style={styles.details}>
											{
												proper.compound_property_type
													.compound.area.name
											}
										</Text>
										<Text style={styles.details}>
											By:{" "}
											{
												proper.compound_property_type
													.compound.developer.name
											}
										</Text>
										<Text style={styles.area}>
											{" "}{proper.max_unit_area} m{" "}
											<Text style={styles.power}>2</Text>
										</Text>
										<Text style={styles.price}>
											{" "}{proper.price_text}{" "}
											{proper.currency_text}
										</Text>
										<Text style={styles.button}>
											{" "}VIEW PROPERTY
										</Text>
									</Image>
									</TouchableOpacity>
								</View>
							)}
						</Swiper>	
					</View>
				</ScrollView>

				<TouchableHighlight
					onPress={() =>
						Communications.phonecall("00201227394544", true)}
					style={styles.touch}
				>
					<Image
						style={{ width: 50, height: 50 }}
						source={require("../resources/images/phone.png")}
					/>
				</TouchableHighlight>
			</View>
		);
	}
}

export const Stack = StackNavigator({
	Details1: { screen: PropertyDetails },
	Details: { screen: PropertyDetails }
});

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	imageText: {
		width: responsiveWidth(99.5),
		flex: 1,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "flex-start",
		margin: 5
	},
	titles: {
		fontWeight: 'bold',
		fontSize: responsiveFontSize(4)
	},
	img: { width: 35, height: 35, marginLeft: 10, margin: 15 },
	fixed: {},
	main: {
		flex: 1
	},
	bed: {
		flexDirection: "row"
	},
	area: {
		flexDirection: "row"
	},
	map: {
		alignItems: "center"
	},
	box1: {
		flex: 0.3,
		backgroundColor: "lightgray"
	},
	//content
	box2: {
		flex: 10,
		backgroundColor: "lightgray",
		margin: 5
	},
	//footer
	box3: {
		flex: 1,
		margin: 5,
		backgroundColor: "lightgray"
	},
	box4: {
		flex: 0.14,
		backgroundColor: "lightgray"
	},
	wrapper: {
		width: responsiveWidth(99.5),
		height: 300
	},
	touch: {
		position: "absolute",
		marginRight: 0,
		width: 50,
		height: 50,
		backgroundColor: "green",
		borderRadius: 25,
		borderColor: "green",
		right: 10,
		bottom: 20
	},
	header: {
		marginTop: -200
	},
	image: {
		height: 250,
		margin: 10
	},
	property: {
		color: "white",
		fontSize: 18,
		fontWeight: "bold",
		marginLeft: 5,
		marginTop: 10
	},
	details: {
		color: "white",
		marginLeft: 5
	},
	price: {
		color: "white",
		fontWeight: "bold",
		marginTop: 20
	},
	area: {
		fontSize: 15,
		marginTop: 50,
		marginLeft: 5,
		fontWeight: "bold",
		color: "white"
	},
	power: {
		fontSize: 10
	},
	button: {
		alignItems: "center",
		justifyContent:'center',
		fontWeight:'bold',
		marginLeft: 200,
		padding: 3,
		marginRight:10,
		color: "white",
		backgroundColor: "#d35400"
	}
});

// {this.state.details.images.map((image, index) =>
// 	<Image
// 		key={index}
// 		source={{
// 			uri:
// 				"http://www.cooingestate.com/" +
// 				image.image_path
// 		}}
// 		style={{ height: 230, margin: 10 }}
// 	/>
// )}

// <ImageSlider
// 	height={200}
// 	images={this.state.details.images.map(
// 		image =>
// 			"http://www.cooingestate.com/" +
// 			image.image_path
// 	)}
// 	position={this.state.position}
// 	onPositionChanged={position =>
// 		this.setState({ position })}
// />
