import React from 'react';
import { StyleSheet, Text, View, ScrollView,Button, Image, TouchableOpacity } from 'react-native';
import { TabNavigator } from "react-navigation";
import { Search } from './Search';
import { ContactUs } from './ContactUs';
import { Header } from './Header';
import Swiper from "react-native-swiper";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import { PropertyDetails } from './PropertyDetails';
import { DeveloperDetails } from './DeveloperDetails';
import { CompoundDetails } from './CompoundDetails';
import { HowItWorks } from "./HowItWorks";
import { AreaDetails } from './AreaDetails';
import _ from 'lodash';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

export class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			propers: [],
			Areas :[],
			Developers: [],
			Compounds: [],
		}
	}

	static navigationOptions = {
		tabBarIcon: () => (
			<Image 
				source={require("../resources/images/home.png")}
				style={[styles.icon]}
			/>
		)
	}

	componentWillMount() {
		fetch("http://192.168.1.20:3000/properties", {
			headers: {
				Accept: "application/json"
			}
		})
			.then(res => res.json())
			.then(propers => {this.setState({ propers })

				var Developers = this.state.propers.map((proper)=>
					proper.compound_property_type.compound.developer)

				Developers = _.uniqBy(Developers, 'name');

                var Compounds = this.state.propers.map((proper)=>{
                   var com =proper.compound_property_type.compound
                   com.image_path =proper.images[0].image_path
                   return com
                   })

                Compounds = _.uniqBy(Compounds, 'name');

	    		var Areas = this.state.propers.map((proper)=>{
                var area = proper.compound_property_type.compound.area
                area.image_path = proper.image_path
                return area
                 })

	    		// var ar = proper.compound_property_type.compound.area
	    		//console.log(ar) ==> {name: "world"}
	    		// ar.img = "sdafsdaf"

	    		// [{name: "hello", img: "http://sdafads"}, {name: "world"}]

               Areas = _.uniqBy(Areas, 'name');

               this.setState({
               		Developers: Developers,
                	Areas: Areas,
                	Compounds: Compounds,
               });

			});
	}

	render(){
		return (
			<View style={styles.container}>
				<ScrollView>
					<View style={styles.weHave}>
	                    <Text style={styles.textPart}>{"We Have"}</Text>
	                    <View style={styles.textContainer}>
	                        <Text style={styles.textPart}>{'137\nCOMPOUNDS'}</Text>
	                        <Text>{"\t"}</Text>
	                        <Text style={styles.textPart}>{'1301\nPROPERTIES'}</Text>
	                        <Text>{"\t"}</Text>
	                        <Text style={styles.textPart}>{'50\nDEVELOPERS'}</Text>
	                    </View>
	            		<View style={{flexDirection: 'row'}}>
		                    <TouchableOpacity style={styles.opacity}
		                    	onPress={() =>
									this.props.navigation.navigate("HowItWorks")}
		                    >
		                        <Text style={styles.howItWorks}>{"How It Works"}</Text>
		                    </TouchableOpacity>
		                     <TouchableOpacity style={styles.opacity}
		                    	onPress={() =>
									this.props.navigation.navigate("Search")}
		                    >
		                        <Text style={styles.search}>{"SEARCH"}</Text>
		                    </TouchableOpacity>
	                    </View>
	                </View>
	                <View style={{alignItems:'center'}}>
						<Text style={{fontSize:20,fontWeight:'bold',margin:5}}>DISCOVER BY AREA</Text>
					</View>
					<View style={{backgroundColor:'white', margin:5 , alignItems:'center'}}>
                        <Swiper
                            style={{width: responsiveWidth(100),
                            		height: 350,
                            		marginRight: 10,}}
                            showsButtons={true}
                            autoplay={true}
                            >
                            {this.state.Areas.map((area, i) =>
                                <View key={i}>
                                    <TouchableOpacity
                                        style={styles.opacity}
                                        onPress={() =>
                                            this.props.navigation.navigate(
                                                "AreaDetails",area
                                            )}
                                        activeOpacity={1}    
                                    >
                                    <Text style={styles.text} >{area.name}</Text>
                                        <Image
                                            source={{ uri:area.image_path}}
                                            style={styles.image}
                                        >                                            
                                            <Text style={styles.buttonarea}>
                                                {"Explore Now"}
                                            </Text>
                                        </Image>
                                    </TouchableOpacity>
                                </View>
                            )}
                        </Swiper>
                    </View>
                     <View style={{alignItems:'center'}}>
						<Text style={{fontSize:20,fontWeight:'bold',margin:5}}>DISCOVER BY COMPOUND</Text>
					</View>
					<View style={{backgroundColor:'white', margin:5 , alignItems:'center'}}>
                        <Swiper
                            style={{width: responsiveWidth(100),
                            		height: 350,
                            		marginRight: 10,}}
                            showsButtons={true}
                            autoplay={true}
                            >
                            {this.state.Compounds.map((compound, i) =>
                                <View key={i}>
                                    <TouchableOpacity
                                        style={styles.opacity}
                                        onPress={() =>
                                            this.props.navigation.navigate(
                                                "CompoundDetails",compound
                                            )}
                                        activeOpacity={1}    
                                    >
                                    <Text style={styles.text} >{compound.name}</Text>
                                    
                                        <Image
                                            source={{ uri:
											"http://www.cooingestate.com/" +
											compound.image_path }}
                                            style={styles.image}
                                            //style={{flex:1,height:300,width:null,margin:5,resizeMode: 'contain'}}
                                        >                                            
                                            <Text style={styles.buttonarea}>
                                                {"Explore Now"}
                                            </Text>
                                        </Image>
                                    </TouchableOpacity>
                                </View>
                            )}
                        </Swiper>
                    </View>
	                <View style={{alignItems:'center'}}>
						<Text style={{fontSize:20,fontWeight:'bold',margin:5}}>TOP DEVELOPERS</Text>
					</View>
					<View style={{backgroundColor:'white', margin:3}}>
					<Swiper
						style={{height:200,margin:5}}
						showsButtons={true}
						autoplay={true}
					>
						{this.state.Developers.map((developer, i) =>
								<View key={i}>
									<TouchableOpacity
										style={{height:150,margin:5}}
										onPress={() =>
											this.props.navigation.navigate(
												"DeveloperDetails",developer
											)}
										activeOpacity={1}	
									>
										<Image
											source={{ uri:
											"http://www.cooingestate.com/" +
											developer.logo_path }}
											style={{flex:1,height:null,width:null,margin:5,resizeMode: 'contain'}}
										>
										</Image>
									</TouchableOpacity>
								</View>
							)}
						</Swiper>	
					</View>
                    <View style={{alignItems:'center'}}>
					<Text style={{fontSize:20,fontWeight:'bold',margin:5}}>FEATURED PROPERTIES</Text>
					</View>
					<View style={{backgroundColor:'white', margin:3}}>
						<Swiper
							style={styles.wrapper}
							showsButtons={true}
							autoplay={true}
							>
							{this.state.propers.map((proper, i) =>
								<View key={i}>
									<TouchableOpacity
										onPress={() =>
											this.props.navigation.navigate(
												"Details",
												{
													id: proper.id,
													compound: proper.compound_property_type.compound.id,
												}
											)}
										activeOpacity={1}	
									>
										<Image
											source={{ uri: proper.image_path }}
											style={styles.image1}
										>
											<Text style={styles.property}>
												{proper.name.toUpperCase()}
											</Text>
											<Text style={styles.details}>
												{proper.compound_property_type.compound.name.toUpperCase()}
											</Text>
											<Text style={styles.details}>
												{
													proper.compound_property_type
														.compound.area.name
												}
											</Text>
											<Text style={styles.details}>
												By:{" "}
												{
													proper.compound_property_type
														.compound.developer.name
												}
											</Text>
											<Text style={styles.area}>
												{" "}{proper.max_unit_area} m{" "}
												<Text style={styles.power}>2</Text>
											</Text>
											<Text style={styles.price}>
												{" "}{proper.price_text}{" "}
												{proper.currency_text}
											</Text>
											<Text style={styles.button}>
												{" "}VIEW PROPERTY
											</Text>
										</Image>
									</TouchableOpacity>
								</View>
							)}
						</Swiper>
					</View>
				</ScrollView>
			</View>

		);
	}
}

export const StackHome = StackNavigator({
  Home: {screen: Home,
    navigationOptions:()=>({
    header: ({ navigation }) => (
        <View style={styles.container2}>
          <TouchableOpacity onPress={() =>  navigation.navigate('DrawerOpen')}>
            <Image style={{width:50, height:50}} source={require("../resources/images/menu2.png")}/>
          </TouchableOpacity>
          <View style={styles.img}>
            <Image source={require("../resources/images/logo.png")} />
          </View>
        </View>
    )}
    )},
  Details: { screen: PropertyDetails,
  navigationOptions:()=>({
    tabBarIcon: () => (
      <Image 
        source={require("../resources/images/home.png")}
        style={styles.icon}/>
    )
    })
    },
  DeveloperDetails: { screen: DeveloperDetails,
  navigationOptions:()=>({
    tabBarIcon: () => (
      <Image 
        source={require("../resources/images/home.png")}
        style={styles.icon}/>
    )
    })
    },
  HowItWorks: { screen: HowItWorks,
  navigationOptions:()=>({
    tabBarIcon: () => (
      <Image 
        source={require("../resources/images/home.png")}
        style={styles.icon}/>
    )
    })
    },
  AreaDetails: { screen: AreaDetails,
  navigationOptions:()=>({
    tabBarIcon: () => (
      <Image 
        source={require("../resources/images/home.png")}
        style={styles.icon}/>
    )
    })
    },
  CompoundDetails: { screen: CompoundDetails,
  navigationOptions:()=>({
    tabBarIcon: () => (
      <Image 
        source={require("../resources/images/home.png")}
        style={styles.icon}/>
    )
    })
    },
  Search: { screen: Search,
  navigationOptions:()=>({
    tabBarIcon: () => (
      <Image 
        source={require("../resources/images/home.png")}
        style={styles.icon}/>
    ),
    header: ({ navigation }) => (
        <View style={styles.container2}>
          <TouchableOpacity onPress={() =>  navigation.navigate('DrawerOpen')}>
            <Image style={{width:50, height:50}} source={require("../resources/images/menu2.png")}/>
          </TouchableOpacity>
          <View style={styles.img}>
            <Image source={require("../resources/images/logo.png")} />
          </View>
        </View>
    )
    }
    )},
});

const styles = StyleSheet.create({
	container: {
	    flex: 1,
	    backgroundColor:'lightgrey',
	},
	icon: {
		width: 26,
		height: 26
	},
	header: {
		marginTop: -200
	},
	image1: {
		height: responsiveHeight(37),
		margin: 10
	},
	property: {
		color: "white",
		fontSize: responsiveFontSize(2.5),
		fontWeight: "bold",
		marginLeft: 5,
		marginTop: 10
	},
	details: {
		color: "white",
		marginLeft: 5
	},
	price: {
		color: "white",
		fontWeight: "bold",
		marginTop: 20
	},
	area: {
		fontSize: responsiveFontSize(2),
		marginTop: 50,
		marginLeft: 5,
		fontWeight: "bold",
		color: "white"
	},
	power: {
		fontSize: responsiveFontSize(1.5)
	},
	button: {
		alignItems: "center",
		justifyContent:'center',
		fontWeight:'bold',
		marginLeft: 200,
		padding: 3,
		marginRight:10,
		color: "white",
		backgroundColor: "#d35400"
	},
	wrapper: {
		width: responsiveWidth(100),
		height: responsiveHeight(45),
		marginRight: 10,
	},
	wrapper1: {
		width: responsiveWidth(100),
		height: responsiveHeight(40),
		marginRight: 10,
	},
	container2: {
	    flexDirection: "row",
	    height: responsiveHeight(10),
	    alignItems: "center",
	    backgroundColor: "#0080ff"
 	},
  	more: {
    	flex: 1
  	},
  	img: {
    	flex: 1,
    	marginLeft:80
  	},
	howItWorks:{
  		backgroundColor: '#d35400',
  		padding:10,
  		color:'white',
  		fontWeight:'bold',
  		fontSize:responsiveFontSize(2.5)
  	},
  	opacity:{
  		margin:10
  	},
  	weHave:{
        backgroundColor: 'white',
        alignItems: 'center',
        marginHorizontal:5
    },
    textContainer:{
        flexDirection:'row' ,
    },
	howItWorks:{
         backgroundColor: '#bdc3c7',
         padding:10,
         color:'white',
         fontWeight:'bold',
         fontSize:responsiveFontSize(2)
     },
     search:{
         backgroundColor: '#d35400',
         padding:10,
         color:'white',
         fontWeight:'bold',
         fontSize:responsiveFontSize(2)
     },
     opacity:{
         margin:10
     },
     textPart:{
        fontSize:responsiveFontSize(2),
        color:"#34495e",
        opacity: 0.9,
        textAlign: 'center',
        paddingVertical:5
    },
    buttonarea: {
        alignItems: "center",
        justifyContent:'center',
        padding:10,
        color:'white',
        fontWeight:'bold',
        fontSize:responsiveFontSize(2),
        backgroundColor: "#d35400", 
    },
    image: {
        height: responsiveHeight(37),
        margin: 10 ,
        justifyContent:'center',
        alignItems:'center'
    },
	text:{
         padding:10,
         color:'black',
         fontWeight:'bold',
         fontSize:20,
         marginLeft:15
     }
})
















	



// var Developers =[]; 
// 		this.state.propers.map((proper)=>{ 
// 			if(!proper.compound_property_type.compound.developer.name in Developers)
// 			 	Developers.push({key: proper.compound_property_type.compound.developer.name,value: proper})
// 		});	

// 		this.setState({
// 	      Developers: Developers
// 	    });					
// >>>>>>> 03fe418590eacbd193f24f53f1af38743d94ba2e



// <Swiper
// 							style={styles.wrapper1}
// 							showsButtons={true}
// 							autoplay={true}
// 							>
// 							{this.state.propers.map((proper, i) =>
// 								<View key={i}>
// 									<TouchableOpacity
// 										style={styles.image}
// 										onPress={() =>
// 											this.props.navigation.navigate(
// 												"Details",
// 												{
// 													id: proper.id,
// 													compound:
// 														proper
// 															.compound_property_type
// 															.compound.name
// 												}
// 											)}
// 										activeOpacity={1}	
// 									>
// 										<Image
// 											source={{ uri:
// 											"http://www.cooingestate.com/" +
// 											proper.compound_property_type.compound.developer.logo_path }}
// 											style={{height:200,width:400}}
// 										>
// 										</Image>
// 									</TouchableOpacity>
// 								</View>
// 							)}
// 						</Swiper>
