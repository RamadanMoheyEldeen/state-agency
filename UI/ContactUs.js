import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  TouchableOpacity,TouchableHighlight ,
  TextInput ,Animated, Keyboard,ScrollView, Platform ,ToastAndroid
} from "react-native";
import * as  t from 'tcomb-form-native';
import { ImagePicker } from "expo";
import { Header } from "./Header";
import ModalFilterPicker from "react-native-modal-filter-picker";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import * as EmailValidator from 'email-validator';
import CameraRollPicker from 'react-native-camera-roll-picker';
import _ from 'lodash';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';



const Form = t.form.Form ;
const phone = t.refinement(t.Number, function (n) { return n.toString().length >= 11 });
const email = t.refinement(t.String, function (mail) { return EmailValidator.validate(mail); });
const  sell = t.struct({
    fullName: t.String,
    phone: phone,
    email:t.maybe(email) ,
    bestTimeToCall:t.String,
    message: t.String,
    });

const stylesheet = _.cloneDeep(t.form.Form.stylesheet);

stylesheet.textbox.normal.height= responsiveHeight(7)



const options = {  auto: 'none' ,  
                  fields: {
                  fullName: {
                  	label:' Full Name:',
                    placeholder: ' Full Name ',
                    returnKeyType: 'next'
                  },
                  phone:{
                  	label: ' Mobile Number:',
                    placeholder:' Your Phone Number (max 14)',
                    error: " Enter a valid phone number",
                    returnKeyType: 'next',
                    maxLength: 14 ,
                   },
                  email:{
                  	label:'E-mail:',
                    placeholder:' Your Email (Optional)',
                    keyboardType: 'email-address',
                    error: 'Insert a valid email' ,
                    returnKeyType: 'next',

                  },
                  bestTimeToCall:{
                  	label:' Best time to call:',
                    placeholder:' Date And Time',
                    returnKeyType: 'next',
                  },
                  message:{
                  	label:'Your Message :',
                  	placeholder:'I am asking about...',
                  	returnKeyType: 'go',
   					stylesheet: stylesheet,
   					multiline:true,
   					numberOfLines: 5

   					}
                }
    };

export class ContactUs extends React.Component {
  constructor() {
    super();
    let index = 0;
    this.state = {
      keyboardHeight: new Animated.Value(0),
      flag: false,
      warning: "",
      value: {} ,
      confirm:"",

    };
  }


      getInitialState() {
      return {
        value: {
            fullName:"",
            phone: "" ,
            email: "",
            bestTimeToCall:"",
            message:""
        }
      };
    }

   onChange(value) {
      console.log(value);
        this.setState({value : value});
  }


    animateKeyboardHeight = (toValue, duration) => {
    Animated.timing(
      this.state.keyboardHeight,
      {toValue, duration},
    ).start();
  };
  static navigationOptions = {
    tabBarIcon: () => (
      <Image 
        source={require("../resources/images/contact.png")}
        style={[styles.icon]}
      />
    )
  }


 
  clearForm() {
    // clear content from all textbox
    this.setState({ value: null });
  }

  onPress(){
    // call getValue() to get the values of the form
    const value = this.refs.form.getValue();
    if(this.state.compoundValue ==  "Select Compound" || this.state.propertyValue == "Select Property Type"){
      this.setState({warning: "Please Choose Compound and Type"})
    }
    else
    {
      this.setState({flag:true})
      this.setState({warning: ""})
    }

    if (value && this.state.flag) { // if validation fails, value will be null
      console.log(value); // value here is an instance of Person
     this.clearForm()
     this.setState({compoundValue: "Select Compound"})
     this.setState({propertyValue: "Select Property Type"})
     this.setState({warning: ""})
     this.setState({flag: false})
     ToastAndroid.show(' Your form has been submitted successfully ', ToastAndroid.SHORT , ToastAndroid.TOP);


    }


  }
  
  render() {
    return (
    <View style={styles.container}>
        <View style={styles.header}>
          <Header navigation={this.props.navigation}/>
        </View>
      <KeyboardAwareScrollView
              extraScrollHeight={60}
              style={styles.container}
              enableOnAndroid
              extraHeight={Platform.OS === "android" ? 10 : undefined}
              enableResetScrollToCoords 
          >
        <View style={styles.about_owner}>
             <Form
                  ref="form"
                  type={sell}
                  options={options}
                  value={this.state.value}
                  context={{locale: 'it-IT'}}
                  onChange={(d)=> this.onChange(d)}
             />

         <Text style={styles.confirm}>{this.state.confirm}</Text>
          <View style={{ alignItems: "center" }}>
            <TouchableHighlight style={styles.submit} onPress={() => this.onPress() }>
              <Text style={{color: 'white' , fontWeight:'bold' , fontSize: 20}}> Submit </Text>
            </TouchableHighlight>
          </View>
        </View>

        <Animated.View style={{height: this.state.keyboardHeight}}/>
        </KeyboardAwareScrollView>
        </View>

      
    ); // end of return
  } // end of render function





    
} // end of the class compononet

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  firstView: {
    flex: 1
  },
  header: {
    flex: 0.15,
    backgroundColor: "white"
  },
  about_unit: {
    flex: 1.18,
    backgroundColor: "white",
    margin: 5
  },
  about_owner: {
    flex:.9 ,
    backgroundColor: "white",
    padding:15
  },
  text: {
    fontSize: 12,
    paddingTop: 3,
    paddingBottom: 3,
    margin: 5
  },
  title: {
    fontWeight: "bold",
    fontSize: 12,
    paddingTop: 3,
    paddingBottom: 3,
    margin: 5
  },
   userText: {
    margin: 5,
    borderWidth: 1,
    backgroundColor: "white",
    height: 40,
    padding: 10,
    borderRadius: 4
  },
  submit: {
    margin: 5,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#d35400",
    borderRadius: 4,
    width: responsiveWidth(50),
    height: responsiveHeight(10),
    marginBottom: 5
  },
  icon: {
    width: 26,
    height: 26
  } ,
  warning:{
    color:"red",
    fontSize:responsiveFontSize(3),
  }
});


  // <CameraRollPicker callback={this.getSelectedImages} />
