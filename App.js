import React from 'react';
import Expo from 'expo';
import { Font } from 'expo';
import { StyleSheet, Text, View } from 'react-native';
import  Drawer  from './UI/Drawer';
import { StackSearch } from './UI/Search';


function cacheFonts(fonts) {
  return fonts.map(font => Expo.Font.loadAsync(font));
}


export default class App extends React.Component {

  componentWillMount(){
    console.disableYellowBox = true;
    Expo.Font.loadAsync('open-sans', '');
  };

 componentDidMount() {
    Font.loadAsync({
      'open-sans-bold': require('./assets/fonts/Open_Sans/OpenSans-Bold.ttf'),
    });
  }
  render() {
    return (
        <Drawer />
    );
  }
}     

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});




